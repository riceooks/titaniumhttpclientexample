// Create window ( http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.Window )
var win = Titanium.UI.createWindow({
    "backgroundColor" : '#FFFFFF',
});

// Create table view ( http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.TableView )
var tableView = Titanium.UI.createTableView();

// Create HTTP client ( http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.Network.HTTPClient )
var client = Titanium.Network.createHTTPClient({
    // Loading
    "onload" : function() {
	// HTTP Status code 200 OK
	if (this.status == 200) {
	    // Response
	    var response = JSON.parse(this.responseText);

	    // Parse Data
	    var data = [];
	    for (i in response.feed.entry) {
		var row = response.feed.entry[i];
		data.push({
		    "title" : row.title.$t,
		    "color" : '#000000',
		});
	    }
	    tableView.data = data;
	    win.add(tableView);
	}
    },
    // Error
    "onerror" : function() {

    },
    // Time
    "timeout" : 5000,
});

// Google Example Calendar ( https://developers.google.com/gdata/samples/cal_sample )
var account = 'developer-calendar@google.com';
// Open URL
client.open("GET", "http://www.google.com/calendar/feeds/" + account + "/public/full?alt=json");
// Send HTTP
client.send();

// Window open
win.open();